public class IOEstandar {
    
    public static void main(String[] args){
        //Buffer para almacenar la entrada de datos
        byte[] buffer = new byte[255];
        System.out.println("Por favor digite un texto: ");
        try {
            //Capturar datos por teclado
            System.in.read(buffer, 0, 255);
        } catch (Exception e) {
            //TODO: handle exception
            System.err.println("Error");
        }
        
        System.out.println("Texto escrito: "+new String(buffer));

    }

}
